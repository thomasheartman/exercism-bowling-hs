{ pkgs ? import <nixpkgs> {}, ghc ? pkgs.ghc }:

with pkgs;

stdenv.mkDerivation {
  name = "bowling";
  inherit ghc;
  buildInputs = [ cabal-install ];
}
