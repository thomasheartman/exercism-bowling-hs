{-# LANGUAGE NamedFieldPuns #-}

module Bowling
  ( score
  , BowlingError(..)
  ) where

import Debug.Trace

data BowlingError
  = IncompleteGame
  | InvalidRoll
      { rollIndex :: Int
      , rollValue :: Int
      }
  deriving (Eq, Show)

data Frame
  = Open Int Int
  | Spare Int
  | Strike
  | Fill Int
  deriving (Eq, Show)

data RawGame =
  RawGame
    { frameNo :: Int
    , rollNo :: Int
    , rolls :: [Int]
    }

(>>:) :: a -> Either e [a] -> Either e [a]
(>>:) x = fmap (x :)

toFrames :: RawGame -> Either BowlingError [Frame]
-- error conditions
toFrames RawGame {frameNo, rollNo, rolls = (x:_)}
  | x > 10 = Left $ InvalidRoll rollNo x
  | x < 0 = Left $ InvalidRoll rollNo x
  | frameNo >= 10 = Left $ InvalidRoll rollNo x
toFrames RawGame {rolls = []} = Left IncompleteGame
-- final frame
toFrames RawGame {frameNo = 9, rollNo, rolls = [10, a, b]}
  | b > 10 = Left $ InvalidRoll (rollNo + 2) b
  | a == 10 || a + b <= 10 = Right [Strike, Fill a, Fill b]
  | otherwise = Left $ InvalidRoll (rollNo + 2) b
toFrames RawGame {frameNo = 9, rollNo, rolls = [10, a]}
  | a <= 10 = Left IncompleteGame
  | otherwise = Left $ InvalidRoll (rollNo + 1) a
toFrames RawGame {frameNo = 9, rolls = [a, b]}
  | a + b < 10 = Right [Open a b]
toFrames RawGame {frameNo = 9, rollNo, rolls = [a, b, c]}
  | a + b == 10 = Right [Spare a, Fill c]
  | otherwise = Left $ InvalidRoll (rollNo + 2) c
toFrames RawGame {frameNo = 9, rollNo, rolls = [_, _, _, d]} =
  Left $ InvalidRoll (rollNo + 3) d
-- strike
toFrames RawGame {frameNo, rollNo, rolls = (10:rest)} =
  Strike >>: toFrames (RawGame (frameNo + 1) (rollNo + 1) rest)
-- rest
toFrames RawGame {frameNo, rollNo, rolls = (a:b:rest)}
  | a + b > 10 = Left $ InvalidRoll (rollNo + 1) b
  | a + b < 10 = Open a b >>: toFrames (RawGame (frameNo + 1) (rollNo + 2) rest)
  | a + b == 10 = Spare a >>: toFrames (RawGame (frameNo + 1) (rollNo + 2) rest)

data Bonus
  = Spare'
  | Strike'
  | DoubleStrike

type Score = Int

f :: (Maybe Bonus, Int) -> Frame -> (Maybe Bonus, Int)
f (Nothing, points) (Open a b) = (Nothing, points + a + b)
f (Nothing, points) (Spare _) = (Just Spare', points + 10)
f (Nothing, points) Strike = (Just Strike', points + 10)
f (Just Spare', points) (Open a b) = (Nothing, points + 2 * a + b)
f (Just Spare', points) (Spare a) = (Just Spare', points + a + 10)
f (Just Spare', points) Strike = (Just Strike', points + 10 + 10)
f (Just Strike', points) (Open a b) = (Nothing, points + 2 * (a + b))
f (Just Strike', points) (Spare _) = (Just Spare', points + 20)
f (Just Strike', points) Strike = (Just DoubleStrike, points + 20)
f (Just DoubleStrike, points) (Open a b) = (Nothing, points + 3 * a + 2 * b)
f (Just DoubleStrike, points) (Spare a) = (Just Spare', points + a + 20)
f (Just DoubleStrike, points) Strike = (Just DoubleStrike, points + 30)
f (Just DoubleStrike, points) (Fill a) = (Just Strike', points + 2 * a)
f (_, points) (Fill a) = (Nothing, points + a)

calculate :: [Frame] -> Score
calculate = snd . foldl f (Nothing, 0)

verifyGame :: [Int] -> Either BowlingError ()
verifyGame xs =
  if length xs < 10
    then Left IncompleteGame
    else Right ()

score :: [Int] -> Either BowlingError Int
score rolls = do
  frames <- toFrames (RawGame 0 0 rolls)
  _ <- verifyGame rolls
  Right $ calculate $ traceShowId frames
