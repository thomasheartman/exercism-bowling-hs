let
  pkgs = import <nixpkgs> { };

in
  { a = pkgs.haskellPackages.callPackage ./project.nix { };
  }
