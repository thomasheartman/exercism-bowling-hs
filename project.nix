{ mkDerivation, base, hpack, hspec, stdenv }:
mkDerivation {
  pname = "bowling";
  version = "1.2.0.7";
  src = ./.;
  libraryHaskellDepends = [ base ];
  libraryToolDepends = [ hpack ];
  testHaskellDepends = [ base hspec ];
  prePatch = "hpack";
  license = "unknown";
  hydraPlatforms = stdenv.lib.platforms.none;
}
